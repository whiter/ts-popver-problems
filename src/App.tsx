import * as React from "react";
import {  Arrow, Manager, Popper, Target,   } from "react-popper";
import "./App.css";

class App extends React.Component {
  public render() {
    return (
      <div className="my-app">
        <Manager>
          <Target style={{ width: 120, height: 120, background: '#b4da55' }}>
            Target Box
          </Target>
          <Popper placement="left" className="popper">
            Left Content
            <Arrow className="popper__arrow"/>
          </Popper>
          <Popper placement="right" className="popper">
            Right Content
            <Arrow className="popper__arrow"/>
          </Popper>
        </Manager>
      </div>
    );
  }
}

export default App;
